#FROM registry.jetbrains.team/p/prj/containers/projector-idea-c
FROM projectorimages/projector-idea-c:2021.2.3

USER root

RUN userdel projector-user
ARG UNAME=projector-user
ARG UID=33333
ARG GID=33333
RUN groupadd -g $GID -o $UNAME
ENV PROJECTOR_DIR /projector
ENV PROJECTOR_USER_NAME projector-user

RUN true \
# Any command which returns non-zero exit code will cause this shell script to exit immediately:
    && set -e \
# Activate debugging to show execution details: all commands will be printed before execution
    && set -x \
# change user to non-root (http://pjdietz.com/2016/08/28/nginx-in-docker-without-root.html):
    && useradd -m -u $UID -g $GID -d /home/$PROJECTOR_USER_NAME -s /bin/bash $PROJECTOR_USER_NAME \
    && chown -R $PROJECTOR_USER_NAME:$PROJECTOR_USER_NAME /home/$PROJECTOR_USER_NAME \
    && chown -R $PROJECTOR_USER_NAME:$PROJECTOR_USER_NAME $PROJECTOR_DIR/ide/bin \
    && chown $PROJECTOR_USER_NAME:$PROJECTOR_USER_NAME run.sh

RUN apt update && apt install -y wget curl openjdk-11-jdk && \
wget https://downloads.lightbend.com/scala/2.12.15/scala-2.12.15.deb && \
dpkg -i scala-2.12.15.deb && curl -O https://dl.google.com/go/go1.12.7.linux-amd64.tar.gz && \
tar xvf go1.12.7.linux-amd64.tar.gz && chown -R root:root ./go && mv go /usr/local
USER projector-user
